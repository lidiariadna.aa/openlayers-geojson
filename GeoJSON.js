import './style.css';
import Circle from 'ol/geom/Circle.js';
import GeoJSON from 'ol/format/GeoJSON.js';
import {Feature, Map, View} from 'ol';
import {Circle as CircleStyle, Fill, Stroke, Style} from 'ol/style.js';
import {OSM, Vector as VectorSource} from 'ol/source.js';
import {Tile as TileLayer, Vector as VectorLayer} from 'ol/layer.js';

const image = new CircleStyle({
  radius: 5,
  fill: null,
  stroke: new Stroke({color: 'red', width: 1}),
});

const styles = {
  'Point': new Style({
    image: image,
  }),
  'LineString': new Style({
    stroke: new Stroke({
      color: 'green',
      width: 3,
    }),
  }),
  'MultiLineString': new Style({
    stroke: new Stroke({
      color: 'magenta',
      width: 5,
    }),
  }),
  'MultiPoint': new Style({
    image: image,
  }),
  'MultiPolygon': new Style({
    stroke: new Stroke({
      color: 'yellow',
      width: 1,
    }),
    fill: new Fill({
      color: 'rgba(255, 255, 0, 0.1)',
    }),
  }),
  'Polygon': new Style({
    stroke: new Stroke({
      color: 'blue',
      lineDash: [4],
      width: 3,
    }),
    fill: new Fill({
      color: 'rgba(0, 0, 255, 0.1)',
    }),
  }),
  'GeometryCollection': new Style({
    stroke: new Stroke({
      color: 'magenta',
      width: 2,
    }),
    fill: new Fill({
      color: 'magenta',
    }),
    image: new CircleStyle({
      radius: 10,
      fill: null,
      stroke: new Stroke({
        color: 'magenta',
      }),
    }),
  }),
  'Circle': new Style({
    stroke: new Stroke({
      color: 'red',
      width: 2,
    }),
    fill: new Fill({
      color: 'rgba(255,0,0,0.2)',
    }),
  }),
};

const styleFunction = function (feature) {
  return styles[feature.getGeometry().getType()];
};

const geojsonObject = {
  'type': 'FeatureCollection',
  'crs': {
    'type': 'name',
    'properties': {
      'name': 'EPSG:4326',
    },
  },
  'features': [
    {
      'type': 'Feature',
      'geometry': {
        'type': 'Point',
        'coordinates': [-99.12766, 19.42847],
      },
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'LineString',
        'coordinates': [
          [-99.12043, 19.42186],
          [-99.11427, 19.45547],
        ],
      },
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'LineString',
        'coordinates': [
          [-99.14763, 19.45774],
          [-99.17049, 19.39970],
        ],
      },
    },
    { 
      'type': 'Feature',
      'geometry': {
        'type': 'Polygon',
        'coordinates': [
          [
            [-99.12228, 19.45981],
            [-99.12758, 19.46147],
            [-99.13334, 19.46465],
            [-99.13356, 19.46472],
            [-99.14146, 19.46511],
            [-99.14321, 19.46481],
            [-99.14994, 19.46296],
            [-99.15757, 19.46391],
            [-99.15890, 19.46360],
            [-99.16021, 19.46195],
            [-99.16081, 19.46084],
            [-99.16166, 19.46025],
            [-99.16248, 19.45965],
            [-99.16316, 19.45832],
            [-99.16373, 19.45559],
            [-99.16421, 19.44983],
            [-99.16578, 19.44349],
            [-99.16952, 19.43603],
            [-99.17788, 19.42312],
            [-99.17777, 19.41921],
            [-99.18420, 19.40759],
            [-99.17124, 19.40309],
            [-99.17038, 19.39956],
            [-99.15651, 19.40374],
            [-99.13086, 19.40245],
            [-99.13007, 19.40304],
            [-99.12568, 19.40434],
            [-99.12675, 19.40876],
            [-99.12818, 19.41334],
            [-99.12704, 19.42045],
            [-99.12605, 19.42270],
            [-99.12316, 19.44242],
            [-99.12598, 19.44752],
            [-99.12331, 19.45800],
          ],
        ],
      },
    },
    {
      'type': 'Feature',
      'geometry': {
        'type': 'MultiLineString',
        'coordinates': [
          [
            [-99.14593, 19.43706],
            [-99.14611, 19.43517],
          ],
          [
            [-99.14611, 19.43517],
            [-99.14095, 19.43414],
          ],
          [
            [-99.14095, 19.43414],
            [-99.14057, 19.43602],
          ],
          [
            [-99.14057, 19.43602],
            [-99.14593, 19.43706],
          ],
        ],
      },
    },
  ],
};

const vectorSource = new VectorSource({
  features: new GeoJSON().readFeatures(geojsonObject),
});

vectorSource.addFeature(new Feature(new Circle([-99.15373, 19.44471], -19.44475)));

const vectorLayer = new VectorLayer({
  source: vectorSource,
  style: styleFunction,
});

const map = new Map({
  layers: [
    new TileLayer({
      source: new OSM(),
    }),
    vectorLayer,
  ],
  target: 'map',
  view: new View({
    center: [-99.12766, 19.42847],
    projection: "EPSG:4326",
    zoom: 13.5
  }),
});